module.exports = fotno => {
	const sdkCredentials = fotno.registerConfiguration(
		'sdk',
		{
			username: null,
			password: null
		},
		value => value.password && value);

	[
		require('./src/command.upgrade.js')
	].forEach(mod => mod(fotno, sdkCredentials));
};
